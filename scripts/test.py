import pandas as pd
import numpy as np
import joblib
import utils
from utils import *
from scipy.special import softmax
import sys

def soft_vote(preds):
    summed_preds = [[np.sum(preds[:,j][:,i]) for i in range(3)] for j in range(len(preds[0]))]
    return [softmax(np.log(sp)) for sp in summed_preds]

def drop_allnan(data):
    for col in data.columns:
        if data[col].isna().sum() == len(data):
            data = data.drop(columns=col)
    return data

X_train = pd.read_csv('./data/X_train_id.csv')
X_train = drop_allnan(X_train)
columns = X_train.columns.tolist()

preprocessor = joblib.load('./models/preprocessor.joblib')
models = []
for i in range(27):
    models.append(joblib.load('./models/model_' + str(i) + '.joblib')) 

data = pd.read_csv('./data/X_test_id.csv')
impact_vals = {'LOW': 0, 'MODIFIER': 1, 'MODERATE': 1.5, 'HIGH': 2}
encoded_impacts = [impact_vals[imp] for imp in data['IMPACT']]
data = data.drop(columns=['IMPACT'])
data['IMPACT'] = encoded_impacts
data = data[columns]
ids = data['ID'].tolist()
data = data.drop(columns='ID')
for col in data.columns:
    data[col] = data[col].astype(X_train[col].dtype)
data = utils.transform(data, preprocessor)
all_preds = []
for i in range(27):
    preds = models[i].predict(data)
    all_preds.append(preds)

y_pred = soft_vote(np.array(all_preds))
pred = [np.argmax(p) for p in y_pred]

out = []
for i in range(len(preds)):
    out.append([ids[i], ['Neutral','GOF','LOF'][pred[i]], *y_pred[i]])
out = pd.DataFrame(out, columns = ['ID', 'prediction', 'LoGoFunc_Neutral', 'LoGoFunc_GOF', 'LoGoFunc_LOF'])
out.to_csv(sys.argv[1], index=None)