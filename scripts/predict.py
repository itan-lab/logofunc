import pandas as pd
import numpy as np
import joblib
import utils
from utils import *
from scipy.special import softmax
import argparse

def soft_vote(preds):
    summed_preds = [[np.sum(preds[:,j][:,i]) for i in range(3)] for j in range(len(preds[0]))]
    return [softmax(np.log(sp)) for sp in summed_preds]

def drop_allnan(data):
    for col in data.columns:
        if data[col].isna().sum() == len(data):
            data = data.drop(columns=col)
    return data

X_train = pd.read_csv('./data/X_train_id.csv')
X_train = drop_allnan(X_train)
columns = X_train.columns.tolist() 

def predict(file, model_dir, outfile, step=10000):
    preprocessor = joblib.load(model_dir + '/preprocessor.joblib')
    models = []
    for i in range(27):
        models.append(joblib.load(model_dir + '/model_' + str(i) + '.joblib'))
    for data in pd.read_csv(file, chunksize=step):
        impact_vals = {'LOW': 0, 'MODIFIER': 1, 'MODERATE': 1.5, 'HIGH': 2}
        encoded_impacts = [impact_vals[imp] for imp in data['IMPACT']]
        data = data.drop(columns=['IMPACT'])
        data['IMPACT'] = encoded_impacts
        data = data[columns]
        ids = data['ID'].tolist()
        data = data.drop(columns='ID')
        for col in data.columns:
            data[col] = data[col].astype(X_train[col].dtype)
        data = utils.transform(data, preprocessor)
        all_preds = []
        for i in range(27):
            preds = models[i].predict(data)
            all_preds.append(preds)

        y_pred = soft_vote(np.array(all_preds))
        pred = [np.argmax(p) for p in y_pred]

        out = []
        for i in range(len(preds)):
            out.append([ids[i], ['Neutral','GOF','LOF'][pred[i]], *y_pred[i]])
        out = pd.DataFrame(out, columns = ['ID', 'prediction', 'LoGoFunc_Neutral', 'LoGoFunc_GOF', 'LoGoFunc_LOF'])
        out.to_csv(outfile, mode='a+', header=None, index=None)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Predict GOF, LOF, and neutral variants with LoGoFunc.')
    parser.add_argument('-f', '--file', required=True, 
                    help='Path to the annotated variants for prediction.')
    parser.add_argument('-m', '--model_dir', required=True, 
                    help='Path to the directory with the trained LoGoFunc models.')
    parser.add_argument('-o', '--outfile', required=True, 
                    help='Path to the file where the output will be written.')
    parser.add_argument('-s', '--step', type=int, default=10000,
                    help='The number of variants to load into memory at a time. Helpful when memory is limited. (default: 10,000)')                
    args = parser.parse_args()
    predict(args.file, args.model_dir, args.outfile, args.step)